#ifndef UTIL_H
#define UTIL_H

#include<stdbool.h>

#define EARTH_R_M 6731000

bool isDigit(char character);

double haversine(double lat1, double lon1, double lat2, double lon2, int radius);

#endif
