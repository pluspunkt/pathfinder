#ifndef PARSER_H
#define PARSER_H

float getDist(Node* node1, Node* node2);

void parseOSM(Node*** nodes, Building*** buildings, char* filename);

#endif