#ifndef ROUTE_H
#define ROUTE_H

#define MAXCONNECTS 10

typedef struct node{
  char* name;
  long int id;
  
  struct node** nextNodes;
  int* weights;
  char** wayNames;

  int connectedNodes;

  int distance;
  struct node* prevNode;

  float lat, lon;
}Node;

typedef struct building{
  char* name;
  Node* closestNode;

  char* street, *housenumber;
}Building;

Node* emptyNode(char* name);

void connectNodes(Node* node1, Node* node2, int weight, char* wayName);

Node* addNode(Node* node, long id, char* name, int weight, char* wayName);

void fillNodes(Node* node);

int prevNodeIndex(Node* node);

void printPath(Node* node);

void printPathWayNames(Node* node);

void printPathWayNamesNoDuplicates(Node* node);

#endif
