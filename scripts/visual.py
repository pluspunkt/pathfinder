import os
import xml.etree.ElementTree as et
from PIL import Image, ImageDraw
from scipy.interpolate import interp1d

def bounds(root):
    minLat = 180.0
    maxLat = 0.0
    minLon = 180.0
    maxLon = 0.0

    for node in root.findall('node'):
        attribs = node.attrib

        if(float(attribs['lat']) < minLat):
            minLat = float(attribs['lat'])
        if(float(attribs['lat']) > maxLat):
            maxLat = float(attribs['lat'])

        if(float(attribs['lon']) < minLon):
            minLon = float(attribs['lon'])
        if(float(attribs['lon']) > maxLon):
            maxLon = float(attribs['lon'])

    return minLat, maxLat, minLon, maxLon

def mapper(latMapper, lonMapper, lat, lon):
    return (
        float(lonMapper(float(lon))),
        float(latMapper(float(lat))),
    )

def mapNodeCoords(node, latMapper, lonMapper):
    return mapper(latMapper, lonMapper, node.attrib['lat'], node.attrib['lon'])

def getNode(root, id):
    for node in root.findall('node'):
        if(node.attrib['id'] == id):
            return node

def drawNodes(img, draw, root, latM, lonM):
    for node in root.findall('node'):
        draw.point(mapNodeCoords(node, latM, lonM), fill=(0,100,100))

def isType(way, typeK, typeV=None):
    for tag in way.findall('tag'):
        if(typeV != None):
            if(tag.attrib['v'] != typeV):
                continue
        
        if(tag.attrib['k'] == typeK):
            return True
        
    return False

def vType(way, typeK):
    for tag in way.findall('tag'):
        if(tag.attrib['k'] == typeK):
            return tag.attrib['v']
        
    return None

def drawWays(img, root, latM, lonM, typeK, color, drawFunc, width=None):
    for way in root.findall('way'):
        if(not isType(way, typeK)): 
            continue
        
        coords = []
        for nd in way.findall('nd'):
            coords.append(mapNodeCoords(getNode(root, nd.attrib['ref']), latM, lonM))

        if(width != None):
            drawFunc(coords, fill=color, width=8)
        else:
            drawFunc(coords, fill=color)

def drawLanduse(img, draw, root, latM, lonM):
    colorDict = {
        "forest" : (100,255,130),
        "grass" : (200,255,230),
        "meadow" : (200,255,230),        
        "farmland" : (255,255,0),
        "residential" : (200,200,200),
        "commercial" : (25,200,255),
        "allotments" : (0,100,10)
    }

    for way in root.findall('way'):
        if(not isType(way, "landuse")): 
            continue
        
        coords = []
        for nd in way.findall('nd'):
            coords.append(mapNodeCoords(getNode(root, nd.attrib['ref']), latM, lonM))

        v = vType(way, "landuse")

        draw.polygon(coords, fill=colorDict.get(v, (200,200,200)))

def draw(root):
    minLat, maxLat, minLon, maxLon = bounds(root)

    imHeight = 5000
    imWidth = int( (maxLon - minLon) / (maxLat - minLat) * imHeight)

    latMapper = interp1d([minLat, maxLat], [1, imHeight])
    lonMapper = interp1d([minLon, maxLon], [1, imWidth])

    img = Image.new(mode="RGB", size=[imHeight, imWidth], color=(200,200,200))
    draw = ImageDraw.Draw(img)

    drawLanduse(img, draw, root, latMapper, lonMapper)
    drawWays(img, root, latMapper, lonMapper, "building", (100,0,0) ,draw.polygon)
    drawWays(img, root, latMapper, lonMapper, "highway", (255,255,255) ,draw.line, 7)

    img.save("test.png")


def main(filename):
    tree = et.parse(filename)
    root = tree.getroot()

    draw(root)
    
main("../map.osm")