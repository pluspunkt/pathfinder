#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include<stdbool.h>

#include"route.h"
#include"util.h"
#include"interface.h"
#include"parser.h"

bool strCmpMinLen(char* str1, char* str2){
    return (strncmp(str1, str2, (strlen(str1)<strlen(str2)) ? strlen(str1):strlen(str2)-1)==0);
}

int printMatchingStreets(Building*** buildings, char* name){
    int n = 0;
    bool exists;

    for(int i = 0; (*buildings)[i]; i++){
        if(strCmpMinLen((*buildings)[i] -> street, name)){
            exists = false;
            for(int o = i - 1; o != 0; o--)
                if(strcmp((*buildings)[i] -> street, (*buildings)[o] -> street)==0)
                    exists = true;
            
            if(!exists)
                printf("%d) %s\n", ++n, (*buildings)[i] -> street);
        }
    }

    return n;
}

char* getNthMatchingStreet(Building*** buildings, char* name, int index){
    int n = 0;
    bool exists;

    for(int i = 0; (*buildings)[i]; i++){
        if(strCmpMinLen((*buildings)[i] -> street, name)){
            exists = false;
            for(int o = i - 1; o != 0; o--)
                if(strcmp((*buildings)[i] -> street, (*buildings)[o] -> street)==0)
                    exists = true;
            
            if(!exists)
                if(++n == index)
                    return (*buildings)[i] -> street;
        }
    }

    return NULL;
}

int printMatchingHouses(Building*** buildings, char* housenumber, char* street){
    int n = 0;

    for(int i = 0; (*buildings)[i]; i++)
        if(strCmpMinLen((*buildings)[i] -> street, street) && strCmpMinLen((*buildings)[i] -> housenumber, housenumber))
            printf("%d) %s %s\n", ++n, (*buildings)[i] -> street, ((*buildings)[i] -> housenumber));

    return n;
}

Building* getNthMatchingHouse(Building*** buildings, char* housenumber, char* street, int index){
    int n = 0;

    for(int i = 0; (*buildings)[i]; i++)
        if(strCmpMinLen((*buildings)[i] -> street, street) && strCmpMinLen((*buildings)[i] -> housenumber, housenumber))
            if(++n == index)
                return (*buildings)[i];

    return NULL;
}


Building* getBuilding(Building*** buildings, char* message){
    char buffer[BUFSIZ];
    char buffer2[BUFSIZ];
    int index, matchCount;
    
    do{
        printf("%s\nEnter the name of the street.\n", message);
        fgets(buffer,BUFSIZ,stdin);
        matchCount = printMatchingStreets(buildings, buffer);
    }while(!matchCount);

    do{
        printf("Please enter a number between 1 and %d\n", matchCount);
        fgets(buffer2,BUFSIZ,stdin);
        index = atol(buffer2);
    }while(index > matchCount || index < 1);

    char* street = getNthMatchingStreet(buildings, buffer, index);
    printf("Your street: %s\n", street);

    do{
        printf("Enter the housenumber.\n");
        fgets(buffer,BUFSIZ,stdin);
        matchCount = printMatchingHouses(buildings, buffer, street);
    }while(!matchCount);

    do{
        printf("Please enter a number between 1 and %d\n", matchCount);
        fgets(buffer2,BUFSIZ,stdin);
        index = atol(buffer2);
    }while(index > matchCount || index < 1);

    return getNthMatchingHouse(buildings, buffer, street, index);
}

double pathLenght(Node* node){
    double dist = 0.0f;
    
    while(node -> prevNode){
        dist += getDist(node, node -> prevNode);
        node = node -> prevNode;
    }

    return dist;
}

typedef struct operator{
    char* streetName;
    struct operator* nextOp;
    float distance;
}Operator;

Operator* pushToStack(Operator* stackTop, char* streetName){
    Operator* newST = calloc(1, sizeof(Operator));
    newST -> streetName = streetName;
    newST -> nextOp = stackTop;
    return newST;
}

Operator* getOperatorStack(Node* destination){
    Node* currNode = destination;

    Operator* stackTop = calloc(1, sizeof(Operator));
    stackTop -> streetName = currNode -> wayNames[prevNodeIndex(currNode)];

    while(currNode -> prevNode){
        if(strcmp(currNode -> wayNames[prevNodeIndex(currNode)], stackTop -> streetName)==0){
            stackTop -> distance += getDist(currNode, currNode -> prevNode);
            currNode = currNode -> prevNode;
            continue;
        }   

        stackTop = pushToStack(stackTop, currNode -> wayNames[prevNodeIndex(currNode)]);
    }

    return stackTop;
}

void printStack(Operator* stackTop){
    while(stackTop -> nextOp){
        printf("%s -> ", stackTop -> streetName);
        stackTop = stackTop -> nextOp;
    }
    printf("%s\n", stackTop -> streetName);
}

void stepByStepNavigation(Operator* stackTop){
    while(stackTop -> nextOp){
        printf("Drive on %s for %d meters, then turn to %s.", stackTop -> streetName, (int) stackTop -> distance, stackTop -> nextOp -> streetName);
        stackTop = stackTop -> nextOp;
        getchar();
    }

    printf("Drive on %s for %d meters to reach your destination.\n", stackTop -> streetName, (int) stackTop -> distance);
}

void interface(Building*** buildings){    
    Building* startingPoint = getBuilding(buildings, "Please specify your starting point.");
    Building* destination = getBuilding(buildings, "Please specify your destination.");
    
    if(!startingPoint || !destination) return;
    
    startingPoint -> closestNode -> distance = 0;
    fillNodes(startingPoint -> closestNode);

    printf("The route has a lenght of %d meters.\n", (int) pathLenght(destination -> closestNode));

    stepByStepNavigation(getOperatorStack(destination -> closestNode));
}