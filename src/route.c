#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<string.h>

#include"route.h"

Node* newNode(){
  Node* newNode = calloc(1, sizeof(Node));
  return newNode;
}

Node* emptyNode(char* name){
  Node* node = newNode();

  node -> name = name;
  node -> nextNodes = calloc(MAXCONNECTS, sizeof(Node*));
  node -> weights = calloc(MAXCONNECTS, sizeof(int));
  node -> wayNames = calloc(MAXCONNECTS, sizeof(char*));

  return node;
}

void connectNodes(Node* node1, Node* node2, int weight, char* wayName){
  if(node1 -> connectedNodes == MAXCONNECTS || node2 -> connectedNodes == MAXCONNECTS)
    return;
  
  node1 -> nextNodes[node1 -> connectedNodes] = node2;
  node1 -> wayNames[node1 -> connectedNodes] = wayName;
  node1 -> weights[node1 -> connectedNodes++] = weight;

  node2 -> nextNodes[node2 -> connectedNodes] = node1;
  node2 -> wayNames[node2 -> connectedNodes] = wayName;
  node2 -> weights[node2 -> connectedNodes++] = weight;
}

Node* addNode(Node* node, long id, char* name, int weight, char* wayName){
  Node* nextNode = emptyNode(name);
  
  nextNode -> id = id;
  nextNode -> distance = INT_MAX;

  connectNodes(nextNode, node, weight, wayName);
  
  return nextNode;
}

void fillNodes(Node* node){
  for(int i = 0; i < (node -> connectedNodes); i++){
    int newDist = node -> distance + node -> weights[i];
    if(node -> nextNodes[i] -> distance > newDist){
      node -> nextNodes[i] -> distance = newDist;
      node -> nextNodes[i] -> prevNode = node;

      fillNodes(node -> nextNodes[i]);
    }
  }
}

int prevNodeIndex(Node* node){
  for(int i = 0; i < node -> connectedNodes; i++)
    if(node -> nextNodes[i] == node -> prevNode)
      return i;

  return -1;
}

void printPath(Node* node){
  if(node -> name) printf("%s", node -> name);

  while((node = node -> prevNode))
    if(node -> name) printf(" <- %s", node -> name);

  printf("\n");
}

void printPathWayNames(Node* node){  
  if(node -> wayNames[prevNodeIndex(node)]) printf("%s", node -> wayNames[prevNodeIndex(node)]);

  while((node = node -> prevNode) -> prevNode)
    if(node -> wayNames[prevNodeIndex(node)]) printf(" <- %s", node -> wayNames[prevNodeIndex(node)]);

  printf("\n");
}

void printPathWayNamesNoDuplicates(Node* node){
  char* prevName = node -> wayNames[prevNodeIndex(node)];
  if(node -> wayNames[prevNodeIndex(node)]) printf("%s", node -> wayNames[prevNodeIndex(node)]);
  
  while((node = node -> prevNode) -> prevNode)
    if(node -> wayNames[prevNodeIndex(node)]){
      if(!prevName) 
        printf(" <- %s", node -> wayNames[prevNodeIndex(node)]);
      else if(strcmp(prevName, node -> wayNames[prevNodeIndex(node)])!=0) 
        printf(" <- %s", node -> wayNames[prevNodeIndex(node)]);
      prevName = node -> wayNames[prevNodeIndex(node)];}

  printf("\n");
}