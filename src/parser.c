#include<stdio.h>
#include<limits.h>
#include<string.h>
#include<libxml/parser.h>
#include<libxml/tree.h>

#include"route.h"
#include"util.h"
#include"parser.h"

#define ELEMENT(X) ((X) -> type == XML_ELEMENT_NODE)
#define XMLNAMECMP(NODE, NDNAME) (xmlStrEqual((NODE)->name, (const xmlChar*) (NDNAME))==1)
//#define XMLPROPCMP(NODE, PROP, CMPVALUE) (xmlStrEqual(xmlGetProp((NODE), (const xmlChar*) (PROP)), (const xmlChar*) (CMPVALUE))==1)

bool xmlPropCmp(xmlNode* node, char* prop, char* cmpValue){
    xmlChar* value = xmlGetProp(node, (const xmlChar*) prop);
    bool output = (bool) xmlStrEqual(value, (const xmlChar*) cmpValue);
    if(value) xmlFree(value);

    return output;
}

int nodeIndexID(long int id, Node*** nodes){
    for(int i = 0; (*nodes)[i]; i++)
        if((*nodes)[i])
            if((*nodes)[i] -> id == id) return i;
    
    return -1;
}

bool isHighway(xmlNode* wayNode){
    for(wayNode = wayNode->children; wayNode; wayNode = wayNode -> next)
        if(ELEMENT(wayNode) && XMLNAMECMP(wayNode, "tag"))
            if(xmlPropCmp(wayNode, "k", "highway")){
                if(xmlPropCmp(wayNode, "v", "residential")) return true; 
                if(xmlPropCmp(wayNode, "v", "living_street")) return true; 
                if(xmlPropCmp(wayNode, "v", "unclassified")) return true; 
            }

    return false;
}

bool isSpecifiedBuilding(xmlNode* wayNode){
    bool found = false;

    for(wayNode = wayNode->children; wayNode; wayNode = wayNode -> next){
        if(!ELEMENT(wayNode) || !XMLNAMECMP(wayNode, "tag")) continue;
            
        if(xmlPropCmp(wayNode, "k", "addr:housenumber"))
            if((found = true)) return true;

        if(xmlPropCmp(wayNode, "k", "addr:street"))
            if((found = true)) return true;   
    }

    return false;
}

int countNodesXML(xmlNode* root){
    int nodes = 0;

    for(xmlNode* currNode = root->children; currNode; currNode = currNode -> next)
        if(ELEMENT(currNode) && XMLNAMECMP(currNode, "node"))
            nodes++;

    return nodes;
}

int countBuildingsXML(xmlNode* root){
    int buildings = 0;
    
    for(xmlNode* currNode = root->children; currNode; currNode = currNode -> next)
        if(ELEMENT(currNode) && XMLNAMECMP(currNode, "way"))
            if(isSpecifiedBuilding(currNode)) buildings++;

    return buildings;
}

int getNodes(xmlNode* root, Node*** nodes){
    int ndCount = 0;
    
    for(xmlNode* currNode = root->children; currNode; currNode = currNode -> next)
        if(ELEMENT(currNode) && XMLNAMECMP(currNode, "node")){
            xmlChar* id = xmlGetProp(currNode, (const xmlChar*) "id");
            xmlChar* lat = xmlGetProp(currNode, (const xmlChar*) "lat");
            xmlChar* lon = xmlGetProp(currNode, (const xmlChar*) "lon");

            (*nodes)[ndCount] = emptyNode(NULL);
            (*nodes)[ndCount] -> id = (id) ? atol((char*) id): (long) ndCount;
            (*nodes)[ndCount] -> lat = (lat) ? atof((const char*) lat): 0.0f;
            (*nodes)[ndCount] -> lon = (lon) ? atof((const char*) lon): 0.0f;
            (*nodes)[ndCount] -> distance = INT_MAX;
            
            if(id) xmlFree(id);
            if(lat) xmlFree(lat);
            if(lon) xmlFree(lon);

            ndCount++;
        }

    return ndCount;
}

char* highwayName(xmlNode* wayNode){
    for(wayNode = wayNode->children; wayNode; wayNode = wayNode -> next)
        if(ELEMENT(wayNode) && XMLNAMECMP(wayNode, "tag"))
            if(xmlPropCmp(wayNode, "k", "name"))
                return (char*) xmlGetProp(wayNode, (const xmlChar*) "v");

    return NULL;
}

int maxspeed(xmlNode* wayNode){
    for(wayNode = wayNode->children; wayNode; wayNode = wayNode -> next)
        if(ELEMENT(wayNode) && XMLNAMECMP(wayNode, "tag"))
            if(xmlPropCmp(wayNode, "k", "maxspeed"))
                return atoi((char*) xmlGetProp(wayNode, (const xmlChar*) "v"));

    return 50;
}

float getDist(Node* node1, Node* node2){
    return haversine(node1 -> lat, node1 -> lon, node2 -> lat, node2 -> lon, EARTH_R_M);
}

void parseWays(xmlNode* root, Node*** nodes){
    long nodeID, prevNodeID = 0;
    char* name = NULL;
    int speedlimit, dist;
    int nodeIndex, prevNodeIndex = -1;
    xmlNode* currNode, *subNode;
    char* tmp;

    for(currNode = root->children; currNode; currNode = currNode -> next)
        if(ELEMENT(currNode) && XMLNAMECMP(currNode, "way")){
            if(!isHighway(currNode)) continue;

            name = highwayName(currNode);
            if(!name) continue;

            speedlimit = maxspeed(currNode);

            for(subNode = currNode->children; subNode; subNode = subNode -> next){
                if(!ELEMENT(subNode) || !XMLNAMECMP(subNode, "nd")) continue;

                tmp = (char*) xmlGetProp(subNode, (const xmlChar*) "ref");
                nodeID = atol(tmp); 
                free(tmp);

                if(prevNodeID){
                    nodeIndex = nodeIndexID(nodeID, nodes);
                    prevNodeIndex = nodeIndexID(prevNodeID, nodes);

                    if(nodeIndex != -1 && prevNodeIndex != -1){
                        dist = (int) getDist((*nodes)[nodeIndex], (*nodes)[prevNodeIndex]);

                        connectNodes((*nodes)[nodeIndex], (*nodes)[prevNodeIndex], dist / speedlimit, name);
                    }
                }
                prevNodeID = nodeID;
            }
            (*nodes)[prevNodeIndex] -> name = name;
        }
}

void getBuildingName(xmlNode* wayNode, Building** building){
    const char* street, *housenumber;    

    for(xmlNode* currNode = wayNode->children; currNode; currNode = currNode -> next)
        if(ELEMENT(currNode) && XMLNAMECMP(currNode, "tag")){
            if(!xmlHasProp(currNode, (const xmlChar*) "k")) continue;
            
            if(xmlPropCmp(currNode, "k", "addr:street"))
                street = (const char*) xmlGetProp(currNode, (const xmlChar*) "v");
            
            if(xmlPropCmp(currNode, "k", "addr:housenumber"))
                housenumber = (const char*) xmlGetProp(currNode, (const xmlChar*) "v");
        }

    (*building) -> street = calloc(strlen(street), sizeof(char) + 1);
    (*building) -> housenumber = calloc(strlen(housenumber), sizeof(char) + 1);
    
    strcpy((*building) -> street, street);
    strcpy((*building) -> housenumber, housenumber);

    int namelen = strlen(street) + strlen(housenumber) + 2;
    (*building) -> name = calloc(namelen, sizeof(char));
    
    snprintf((*building) -> name, namelen, "%s %s", street, housenumber);
}

long int firstWayRef(xmlNode* wayNode){
    xmlChar* firstRefId = NULL;
    
    for(wayNode = wayNode->children; wayNode; wayNode = wayNode -> next)
        if(ELEMENT(wayNode) && XMLNAMECMP(wayNode, "nd")){
            firstRefId = xmlGetProp(wayNode, (const xmlChar*) "ref");
            break;
        }
        
    return (firstRefId) ? atol((char*) firstRefId): -1;
}

void getBuildings(xmlNode* root, Node*** nodes, Building*** buildings, int* ndCount){
    long id;
    int shortestDist;
    int bdCount = 0;
    Node* closestNode = NULL, *bNode;

    for(xmlNode* currNode = root->children; currNode; currNode = currNode -> next){
        if(!ELEMENT(currNode)) continue;
        if(!isSpecifiedBuilding(currNode)) continue;

        shortestDist = INT_MAX;
        id = firstWayRef(currNode);

        if(nodeIndexID(id, nodes) == -1) continue;
        bNode = (*nodes)[nodeIndexID(id, nodes)];

        for(int i = 0; i < (*ndCount); i++){
            if(!(*nodes)[i] -> name) continue;
            if((*nodes)[i] -> id == bNode -> id) continue;
            
            if(getDist((*nodes)[i], bNode) < shortestDist){
                shortestDist = getDist((*nodes)[i], bNode);
                closestNode = (*nodes)[i];
            }
        }

        (*buildings)[bdCount] = calloc(1, sizeof(Building));
        (*buildings)[bdCount] -> closestNode = closestNode;
        getBuildingName(currNode, &(*buildings)[bdCount]);

        bdCount++;
    }
}

void parseOSM(Node*** nodes, Building*** buildings, char* filename){
    xmlDoc* doc = xmlReadFile(filename, NULL, 0);
    if(!doc) return;

    xmlNode* root = xmlDocGetRootElement(doc);
 
    (*nodes) = calloc(countNodesXML(root) + 1, sizeof(Node*));
    (*buildings) = calloc(countBuildingsXML(root) + 1, sizeof(Building*));

    int ndCount = getNodes(root, nodes);
    parseWays(root, nodes);

    getBuildings(root, nodes, buildings, &ndCount);

    xmlFreeDoc(doc);
    xmlCleanupParser();
}