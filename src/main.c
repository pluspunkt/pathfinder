#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<unistd.h>
#include<libxml/parser.h>
#include<libxml/tree.h>
#include<libxml/xmlreader.h>

#include"route.h"
#include"parser.h"
#include"parser.h"
#include"interface.h"

void test(){
  Node* rootNode = emptyNode("Root");
  int nodeId = 1;
  
  Node* node1 = addNode(rootNode, nodeId++, "Node1", 10, NULL);
  Node* node2 = addNode(node1, nodeId++, "Node2", 5, NULL);
  Node* node3 = addNode(node2, nodeId++, "Node3", 7, NULL);

  connectNodes(node1, node3, 8, NULL);
  fillNodes(rootNode);
  printPath(node3);
}

int main(int argc, char** argv){
  char* filename = (argc == 2) ? argv[1]: "map.osm";
  
  Node** nodes = NULL; 
  Building** buildings = NULL;

  parseOSM(&nodes, &buildings, filename);

  interface(&buildings);

  return 0;
}