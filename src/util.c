#include<math.h>

#include"util.h"

double haversine(double lat1, double lon1, double lat2, double lon2, int radius) { 
    double dLat = (lat2 - lat1) * M_PI / 180.0; 
    double dLon = (lon2 - lon1) * M_PI / 180.0; 
    lat1 *= M_PI / 180.0; 
    lat2 *= M_PI / 180.0; 
  
    double a = pow(sin(dLat / 2), 2) +  
                   pow(sin(dLon / 2), 2) *  
                   cos(lat1) * cos(lat2); 
    
    return radius * 2 * asin(sqrt(a)); 
}