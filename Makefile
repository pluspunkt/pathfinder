CC=cc
CFLAGS=-O3 -march=native
SRCDIR=src/
IDIR=include/
LIBXMLFLAGS=`xml2-config --cflags --libs`

output: main.o parser.o route.o util.o interface.o
	${CC} ${LIBXMLFLAGS} main.o parser.o route.o util.o interface.o ${CFLAGS} -o pathfinder.out -lm

main.o: ${SRCDIR}main.c
	${CC} ${LIBXMLFLAGS} -c ${SRCDIR}main.c ${CFLAGS} -I ${IDIR}

parser.o: ${SRCDIR}parser.c ${IDIR}parser.h
	${CC} ${LIBXMLFLAGS} -c ${SRCDIR}parser.c ${CFLAGS} -I ${IDIR}

route.o: ${SRCDIR}route.c ${IDIR}route.h
	${CC} -c ${SRCDIR}route.c ${CFLAGS} -I ${IDIR}

util.o: ${SRCDIR}util.c ${IDIR}util.h
	${CC} -c ${SRCDIR}util.c ${CFLAGS} -I ${IDIR}

interface.o: ${SRCDIR}interface.c ${IDIR}interface.h
	${CC} -c ${SRCDIR}interface.c ${CFLAGS} -I ${IDIR}

clean:
	rm *.o -f
